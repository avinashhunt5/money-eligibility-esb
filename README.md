# money-eligibility-esb

This project is ESB (Enterprise Service Bus) which is build on Spring Boot with Apache CAMEL
Its uses Scatter Gatter EIP Pattern to trigger 2 service in parallel and aggregate the response

1. Service one needs the XML request so to form that request it uses FreeMarker engine to build the request
2. Service two is a get request to call gold asset service


## build using
gradle clean build

## run the post api request on
http://localhost:9898/money-eligibility-esb/esb/processEligibility
body
{
"salary": "1000",
"goldAsset": "25000"
}