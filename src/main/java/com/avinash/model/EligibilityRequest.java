package com.avinash.model;

public class EligibilityRequest {

    private String goldAsset;
    private String salary;

    public String getGoldAsset() {
        return goldAsset;
    }

    public void setGoldAsset(String goldAsset) {
        this.goldAsset = goldAsset;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
