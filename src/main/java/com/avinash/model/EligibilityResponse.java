package com.avinash.model;

public class EligibilityResponse {

    private String message;
    private String goldAssetServiceResponse;
    private String kafkaServiceResponse;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGoldAssetServiceResponse() {
        return goldAssetServiceResponse;
    }

    public void setGoldAssetServiceResponse(String goldAssetServiceResponse) {
        this.goldAssetServiceResponse = goldAssetServiceResponse;
    }

    public String getKafkaServiceResponse() {
        return kafkaServiceResponse;
    }

    public void setKafkaServiceResponse(String kafkaServiceResponse) {
        this.kafkaServiceResponse = kafkaServiceResponse;
    }
}
