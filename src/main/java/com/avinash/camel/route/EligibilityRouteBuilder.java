package com.avinash.camel.route;

import com.avinash.camel.aggregator.EligibilityResponseAggregator;
import com.avinash.camel.processor.EligibilityProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http.HttpMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class EligibilityRouteBuilder extends RouteBuilder {

    public static final String PROCESS_ELIGIBILITY_ROUTE = "direct:processEligibility";

    @Value("${eligibility.kafka.service.url}")
    private String kafkaServiceUrl;

    @Value("${gold-asset.service.url}")
    private String goldAssetServiceUrl;

    @Autowired
    private EligibilityResponseAggregator eligibilityResponseAggregator;

    @Autowired
    private EligibilityProcessor eligibilityProcessor;

    //this route will use scatter-gather EIP pattern
    @Override
    public void configure() throws Exception {

          from(PROCESS_ELIGIBILITY_ROUTE)
                  .routeId("processEligibilityId")
                  .log(LoggingLevel.INFO,"request reached  ${body}")
                  .process(eligibilityProcessor)
                  .log("request formed :: ${body}")
                  .multicast(eligibilityResponseAggregator)
                  .to("seda:kafka-service-route", "seda:gold-asset-service")
                  .end();

          from("seda:kafka-service-route")
                  .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.POST))
                  .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_XML_VALUE))
                  .toD(kafkaServiceUrl);

          from("seda:gold-asset-service")
                  .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.GET))
                  .toD(goldAssetServiceUrl + "?goldAsset=${header.GOLD_ASSET_VALUE}");

    }


}
