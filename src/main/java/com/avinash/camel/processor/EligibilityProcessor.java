package com.avinash.camel.processor;

import com.avinash.model.EligibilityRequest;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class EligibilityProcessor implements Processor {

    private static final Logger logger = LoggerFactory.getLogger(EligibilityProcessor.class);

    @Autowired
    private Configuration config;

    @Override
    public void process(Exchange exchange) throws Exception {
        logger.info("Request processing as processor");
        EligibilityRequest eligibilityRequest = exchange.getIn().getBody(EligibilityRequest.class);
        Template template = config.getTemplate("request.ftl");
        Map<String, Object> model = new HashMap<>();
        model.put("salary", eligibilityRequest.getSalary());
        model.put("goldAsset", eligibilityRequest.getGoldAsset());
        exchange.getIn().setHeader("GOLD_ASSET_VALUE", eligibilityRequest.getGoldAsset());
        String request = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        exchange.getIn().setBody(request);
    }
}
