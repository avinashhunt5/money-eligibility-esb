package com.avinash.camel.aggregator;

import com.avinash.model.EligibilityResponse;
import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;
import org.apache.camel.spi.ExceptionHandler;
import org.springframework.stereotype.Component;

@Component
public class EligibilityResponseAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        EligibilityResponse response = new EligibilityResponse();
        response.setMessage("Failed");

        if(oldExchange == null) {
            return newExchange;
        } else if (oldExchange != null && newExchange !=null) {

            String kafkaServiceResponse = oldExchange.getIn().getBody(String.class);
            response.setKafkaServiceResponse(kafkaServiceResponse);

            String goldAssetResponse = newExchange.getIn().getBody(String.class);
            response.setGoldAssetServiceResponse(goldAssetResponse);

            if (response.getGoldAssetServiceResponse() != null &&
                    response.getKafkaServiceResponse().equalsIgnoreCase("success")) {
                response.setMessage("SUCCESS");
                newExchange.getIn().setBody(response);
            }

        }
        return newExchange;
    }
}
