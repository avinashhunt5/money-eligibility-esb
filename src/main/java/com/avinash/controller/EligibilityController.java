package com.avinash.controller;

import com.avinash.camel.route.EligibilityRouteBuilder;
import com.avinash.model.EligibilityRequest;
import com.avinash.model.EligibilityResponse;
import org.apache.camel.CamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/esb")
public class EligibilityController {

    private static final Logger logger = LoggerFactory.getLogger(EligibilityController.class);

    @Autowired
    private CamelContext camelContext;

    @PostMapping("/processEligibility")
    public ResponseEntity<EligibilityResponse> processEligibility(@RequestBody EligibilityRequest request) {
        EligibilityResponse response = camelContext.createProducerTemplate().requestBody(EligibilityRouteBuilder.PROCESS_ELIGIBILITY_ROUTE,
                request, EligibilityResponse.class);
        return ResponseEntity.ok(response);
    }
}
